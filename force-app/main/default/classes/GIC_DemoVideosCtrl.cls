/**Created By Dhana Prasad to fetch custom meta data of you tube linkes to played on LWC */
public with sharing class GIC_DemoVideosCtrl {
    
    @AuraEnabled
    public static List<GIC_Video_Links__mdt> fetchVideoLinks(){
        try {
          return [SELECT Id, MasterLabel, Sequence_Number__c, Video_URL__c, Thumbnail_URL__c  FROM GIC_Video_Links__mdt ORDER BY Sequence_Number__c ASC];  
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}
