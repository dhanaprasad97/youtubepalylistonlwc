import { LightningElement, track, wire } from 'lwc';
import fetchVideoLinks from '@salesforce/apex/GIC_DemoVideosCtrl.fetchVideoLinks';

/**Created by Dhana Prasad */
export default class GicHomePageVideos extends LightningElement {

    @track mainVideo;
    @track restVideos = [];
    @track allVideos = [];

    connectedCallback(){
        fetchVideoLinks({}).
        then(result=>{
            let count = 0;
            result.forEach(element => {
                element.Video_URL__c += '?modestbranding=1&enablejsapi=1';
                if(count == 0){
                    this.mainVideo =  element
                    this.mainVideo.Video_URL__c += '&autoplay=0&mute=0&rel=0&iv_load_policy=3&disablekb=1&showinfo=0&mode=opaque&autohide=1&wmode=transparent';;
                } else {
                    this.restVideos.push(element);
                }
                this.allVideos.push(element);
                count++;
            });
        }).catch(error=>{});
    }

    playthisvideo(event){
        this.restVideos = [];
        let index = parseInt(event.currentTarget.dataset.index);
        this.allVideos.forEach(element => {
            console.log(element.Sequence_Number__c , index);
            if(element.Sequence_Number__c == index){
                this.mainVideo = Object.assign({}, element);
                this.mainVideo.Video_URL__c+='&autoplay=1&mute=0&rel=0&iv_load_policy=3&disablekb=1&showinfo=0&mode=opaque&autohide=1&wmode=transparent';
            } else {
                this.restVideos.push(element);  
            }
        });

    }

}