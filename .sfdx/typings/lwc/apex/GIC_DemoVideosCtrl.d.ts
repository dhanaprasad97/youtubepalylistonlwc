declare module "@salesforce/apex/GIC_DemoVideosCtrl.fetchVideoLinks" {
  export default function fetchVideoLinks(): Promise<any>;
}
